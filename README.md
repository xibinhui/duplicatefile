# **重复文件清理（duplicatefile）**

#### 一、介绍
​	可根据文件类型、文件大小、文件内容等条件进行查找，体积小，速度快。手机上重复照片的清理，微信中重复照片的清理，都不是事，分分钟为你搞定。
 


#### 二、下载地址
本站下载：[重复文件清理（duplicatefile）](https://gitee.com/xibinhui/duplicatefile/tree/master/bin)

百度网盘下载：[重复文件清理（duplicatefile）](https://pan.baidu.com/s/1QCsV62aI_hOenw4-f0Ocqw?pwd=xibh#list/path=%2Fxibinhui%2Fduplicatefile)



#### 三、安装教程
1. 将下载的文件解压到D:\xibinhui，D:\Program\xibinhui或者其他你规划的目录中。

   注意事项：

   ​	1）请不要解压到桌面。
   ​	2）请不要解压到系统所在的盘符中。
   ​	3）请不要在压缩文件中运行本程序。

2. 我的安装路径为D:\Program\xibinhui\duplicatefile

3. 安装完成。双击duplicatefile.exe即可打开本程序。

   

#### 四、使用说明
1. ​	**主界面**
    ![](https://gitee.com/xibinhui/duplicatefile/raw/master/doc/df000.jpg)
    工具分为六步：
    ​	1）添加路径。添加要查找的目录，工具在这些目录的文件中查找重复的文件。
    ​	2）设置重复文件的规则。有内容相同、大小相同、类型相同等。规则间为并且的关系。
    ​	3）开始查找。需要耐心等几分钟，时间长短由目录中文件个数决定。
    ​	4）在重复的文件组中文件。
    ​	5）处理选择的文件。
    ​	6)结束。

  

2. ​	**选择重复文件**
    ![](https://gitee.com/xibinhui/duplicatefile/raw/master/doc/df001.jpg)
    选择最新创建：在重复的文件组中，创建时间最旧的一个不选，选择其他的。
    选择最旧创建：在重复的文件组中，创建时间最新的一个不选，选择其他的。
    选择最新修改：在重复的文件组中，修改时间最旧的一个不选，选择其他的。
    选择最旧创建：在重复的文件组中，修改时间最新的一个不选，选择其他的。
    选择全部：选择所有文件。一般用于移动或复制操作。

  

3. ​	**处理重复文件**
    ![](https://gitee.com/xibinhui/duplicatefile/raw/master/doc/df002.jpg)
    移动到目录：将选择的文件剪切到指定的目录。
    复制到目录：将选择的文件复制到指定的目录。
    直接删除：删除选择的文件。**删除的文件无法找回，请谨慎操作。**

  

4. ​	**导出列表**
    ![](https://gitee.com/xibinhui/duplicatefile/raw/master/doc/df003.jpg)
    将列表中的文字导出文本文件中。
    
    

#### 五、版本历史
1.  v1.2（2020-11-29）更新发布了，增加目录拖放。

2.  v1.1（2020-10-31）更新发布了，修复了几个小BUG。

3.  v1.0（2020-08-31）发布了。

   

#### 六、支持

1. 软件问题：在线论坛 - [Issues](https://gitee.com/xibinhui/duplicatefile/issues)
2. 功能需求：邮箱 - leashi@163.com
3. 支付故事：QQ号 - 79534262  QQ群 - 314714697



